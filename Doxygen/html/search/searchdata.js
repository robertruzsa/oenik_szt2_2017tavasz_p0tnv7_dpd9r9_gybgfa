var indexSectionsWithContent =
{
  0: "abdfghikmoprstuvw",
  1: "abdfkmstuv",
  2: "bdrw",
  3: "abdfghikmoprsuv",
  4: "u",
  5: "adkostv",
  6: "p"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "properties",
  6: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Properties",
  6: "Events"
};

