﻿namespace WPF
{
    using System.Windows;
    using System.Windows.Input;
    using BLL;
    using DAL;

    /// <summary>
    /// Interaction logic for FizetesWindow.xaml
    /// </summary>
    public partial class FizetesWindow : Window
    {
        /// <summary>
        /// BusinessLogic
        /// </summary>
        private BusinessLogic BL;

        /// <summary>
        /// Paraméter nélküli konstruktor
        /// </summary>
        public FizetesWindow()
        {
            InitializeComponent();
            BL = BusinessLogic.Get();
            this.DataContext = BL.VM;

            int sum = 0;
            foreach (Alkatresz a in BL.VM.OptimalisKonfiguracio)
            {
                sum += (int)a.eladasi_ar;
            }

            lblOsszar.Content = sum;
        }

        /// <summary>
        /// számlaszám ellenörzés
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void txtSzamlaszam_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// pass ellenörzés
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void passErvenyesitesiKod_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Ez a metódus becsukja az ablakot ha ez manual window vagy diagnosztika window
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">r</param>
        private void button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            foreach (Window w in Application.Current.Windows)
            {
                if (w is ManualWindow || w is DiagnosztikaWindow)
                {
                    w.Close();
                }
            }
        }
    }
}
