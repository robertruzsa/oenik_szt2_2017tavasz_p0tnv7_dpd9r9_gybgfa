﻿namespace WPF
{
    using System.Windows;
    using BLL;

    /// <summary>
    /// Interaction logic for DiagnosztikaWindow.xaml
    /// </summary>
    public partial class DiagnosztikaWindow : Window
    {
        /// <summary>
        /// BusinessLogic
        /// </summary>
        private BusinessLogic BL;

        /// <summary>
        /// Paraméter nélküli konstruktor
        /// </summary>
        public DiagnosztikaWindow()
        {
            InitializeComponent();
            BL = BusinessLogic.Get();
            this.DataContext = BL.VM;
            BL.VM.OptimalisKonfiguracio.Clear();
        }

        /// <summary>
        /// fejlesztés gomb
        /// </summary>
        /// <param name="sender">sender param</param>
        /// <param name="e">e param</param>
        private void Fejlesztes_button_Click(object sender, RoutedEventArgs e)
        {
            BL.VM.OptimalisKonfiguracio = BL.Fejlesztes((int)slider.Value);
        }

        /// <summary>
        /// fizetés gomb
        /// </summary>
        /// <param name="sender">sender param</param>
        /// <param name="e">e param</param>
        private void fizetes_Click(object sender, RoutedEventArgs e)
        {
            new FizetesWindow().ShowDialog();
        }
    }
}
