﻿namespace WPF
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for UgyfelWindow.xaml
    /// </summary>
    public partial class UgyfelWindow : Window
    {
        /// <summary>
        /// paraméter nélküli konstruktor
        /// </summary>
        public UgyfelWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// manual ablak
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void button_Click(object sender, RoutedEventArgs e)
        {
            new ManualWindow().ShowDialog();
        }

        /// <summary>
        /// diagnosztika ablak
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            new DiagnosztikaWindow().ShowDialog();
        }
    }
}
