﻿namespace WPF
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using BLL;
    using DAL;
    
    /// <summary>
    /// Interaction logic for AddWindow.xaml
    /// </summary>
    public partial class AddWindow : Window
    {
        /// <summary>
        /// BusinessLogic példány
        /// </summary>
        private BusinessLogic BL;

        /// <summary>
        /// Paraméter néküli konstruktor
        /// </summary>
        public AddWindow()
        {
            InitializeComponent();
            BL = BusinessLogic.Get();
            this.DataContext = BL.VM;
        }

        /// <summary>
        /// új alkatrész hozzáadása
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (txtNev.Text != String.Empty && txtTipus.Text != String.Empty && combo.SelectedItem != null && txtErtek.Text != String.Empty && txtMenny.Text != String.Empty && txtAr.Text != String.Empty )
            {
                BL.UjAlkatresz(txtNev.Text, txtTipus.Text, combo.SelectedItem.ToString(), int.Parse(txtErtek.Text), int.Parse(txtMenny.Text), int.Parse(txtAr.Text));
                BL.VM.AlkatreszekRaktaron.Add(new Alkatresz() { nev = txtNev.Text, alk_tipus = txtTipus.Text, kategoria = combo.SelectedItem.ToString(), ertek = int.Parse(txtErtek.Text), mennyiseg = int.Parse(txtMenny.Text), eladasi_ar = int.Parse(txtAr.Text) });
                this.Close();
            }
            else
            {
                MessageBox.Show("Adja meg a szükséges adatokat!");
            }
        }

        /// <summary>
        /// karakterek ellenörzése
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void txtErtek_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// karakterek ellenörzése
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void txtMenny_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// karakterek ellenörzése
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void txtAr_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;
            }
        }
    }
}
