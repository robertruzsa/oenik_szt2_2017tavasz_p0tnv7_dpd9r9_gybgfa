﻿namespace WPF
{
    using System.Windows;
    using BLL;
    using DAL;

    /// <summary>
    /// Interaction logic for ManualWindow.xaml
    /// </summary>
    public partial class ManualWindow : Window
    {
        #region Fields

        /// <summary>
        /// BL
        /// </summary>
        private BusinessLogic BL;

        #endregion

        #region Constructor

        /// <summary>
        /// A ManualWindow konstruktora
        /// </summary>
        public ManualWindow()
        {
            InitializeComponent();
            BL = BusinessLogic.Get();
            DataContext = BL.VM;
            BL.VM.Alkatreszek = DatabaseMethods.AlkatreszekLekerdezese(BL.VM.KivalasztottAuto.au_tipus, BL.VM.DE);
        }

        #endregion

        #region Events

        /// <summary>
        /// A fizetéshez irányító gomb eseménye
        /// </summary>
        /// <param name="sender">s</param>
        /// <param name="e">e</param>
        private void btnFiz_Click(object sender, RoutedEventArgs e)
        {
            BL.VM.OptimalisKonfiguracio.Clear();
            foreach (var alkatresz in BL.VM.AlkMan)
            {
                if (alkatresz.Szukseges)
                {
                    BL.VM.OptimalisKonfiguracio.Add(alkatresz.Alkatresz);
                }
            }

            new FizetesWindow().ShowDialog();
        }

        #endregion
    }
}
