﻿namespace WPF
{
    using System;
    using System.Windows;
    using BLL;    

    /// <summary>
    /// Interaction logic for BossWindow.xaml
    /// </summary>
    public partial class BossWindow : Window
    {
        /// <summary>
        /// BusinessLogic osztály példánya
        /// </summary>
        private BusinessLogic BL;

        /// <summary>
        /// Paraméter néküli konstruktor
        /// </summary>
        public BossWindow()
        {
            InitializeComponent();
            BL = BusinessLogic.Get();
            this.DataContext = BL.VM;
        }

        /// <summary>
        /// Rendelés
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (BL.VM.KivalasztottAlkatresz != null && mennyTxt.Text != String.Empty)
            {
                BL.VM.AlkatreszekRaktaron = BL.AlkatreszRendeles(BL.VM.KivalasztottAlkatresz, int.Parse(mennyTxt.Text));
            }
            else
            {
                MessageBox.Show("Válasszon ki egy alkatrészt és adja meg a rendelni kívánt mennyiséget!");
            }
        }

        /// <summary>
        /// új alkatrész felvétele
        /// </summary>
        /// <param name="sender">sender param</param>
        /// <param name="e">e param</param>
        private void ujAlk_btn_Click(object sender, RoutedEventArgs e)
        {
            new AddWindow().ShowDialog();
        }
    }
}
