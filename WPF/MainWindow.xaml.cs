﻿namespace WPF
{   
    using System;
    using System.Windows;
    using BLL;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// businesslogix
        /// </summary>
        private BusinessLogic BL;

        /// <summary>
        /// paraméter nélküli könstruktor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            BL = BusinessLogic.Get();
            DataContext = BL.VM;
        }

        /// <summary>
        /// Kezdőlap reg click metódus
        /// </summary>
        /// <param name="sender">sender s</param>
        /// <param name="e">e EventArgs</param>
        private void reg_btn_Click(object sender, RoutedEventArgs e)
        {
            string password = passwordBox.Password;
            try
            {
                if (!DAL.UsersDB.UserIsExisting(email_txt.Text) && BL.VM.KivalasztottAuto != null)
                {
                    DAL.UsersDB.InsertToDB(email_txt.Text, password);
                    SendEmail.SendMail(email_txt.Text, password);
                    MessageBox.Show("Sikeres regisztráció!");
                }
                else if (BL.VM.KivalasztottAuto == null)
                {
                    MessageBox.Show("Válassza ki az autó típusát!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Sikertelen!" + ex.ToString());
            }
        }

        /// <summary>
        /// Kezdőlap log click metódus
        /// </summary>
        /// <param name="sender">sender ss</param>
        /// <param name="e">e EventArgs</param>
        private void log_btn_Click(object sender, RoutedEventArgs e)
        {
            if (email_txt.Text == "admin" && passwordBox.Password == "admin")
            {
                new BossWindow().ShowDialog();
            }
            else if (!DAL.UsersDB.UserIsExisting(email_txt.Text))
            {
                MessageBox.Show("Nincs ilyen user");
            }
            else
            {
                string password = DAL.UsersDB.PasswordFromDB(email_txt.Text);
                if (password == passwordBox.Password)
                {
                    new UgyfelWindow().ShowDialog();
                }
                else
                {
                    MessageBox.Show("Hibás jelszó");
                }
            }
        }

        /// <summary>
        /// Gotfocus megvalositasa emailt kinullazza
        /// </summary>
        /// <param name="sender">sender s</param>
        /// <param name="e">e EventArgs</param>
        private void email_txt_GotFocus(object sender, RoutedEventArgs e)
        {
            email_txt.Text = string.Empty;
        }
    }
}