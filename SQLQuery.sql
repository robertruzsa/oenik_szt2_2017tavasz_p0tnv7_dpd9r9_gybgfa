﻿create table Auto(
au_tipus varchar(10),
constraint PK primary key(au_tipus));

create table Alkatresz(
nev varchar(25),
alk_tipus varchar(100),
kategoria varchar(20),
ertek numeric,
eladasi_ar numeric,
mennyiseg numeric,
constraint PK_alkatresz primary key(nev, alk_tipus));

create table Tartalmaz(
au_tipus varchar(10),
nev varchar(25),
alk_tipus varchar(100),
mennyiseg numeric,
constraint PK_Tartalmaz primary key(au_tipus, nev, alk_tipus),
constraint FK_Tartalmaz_Auto foreign key(au_tipus) references Auto(au_tipus),
constraint FK_Tartalmaz_Alk foreign key(nev, alk_tipus) references Alkatresz(nev, alk_tipus));


insert into Auto values('A200');
insert into Auto values('A180');
insert into Auto values('A250');
insert into Auto values('CLA250');
insert into Auto values('C250');
insert into Auto values('C300');
insert into Auto values('E300');
insert into Auto values('CLS400');
insert into Auto values('SL400');
insert into Auto values('AMG GT');

insert into Alkatresz
values('motor','soros/4/115LE','teljesítmény',4.8,260000,3);
insert into Alkatresz
values('kerék','205/55 R 16','irányíthatóság',4.8,80000,12);
insert into Alkatresz
values('váltó','7G-DCT','gyorsulás',4.8,112000,4);
insert into Alkatresz
values('fék elöl','belső hűtésű tárcsafék','irányíthatóság',5.0,92000,4);
insert into Alkatresz
values('fék hátul','belső hűtésű tárcsafék','irányíthatóság',5.0,92000,4);
insert into Alkatresz
values('fék hátul','tömör tárcsafék','irányíthatóság',5.0,94000,6);
insert into Alkatresz
values('rugózás','tekercsrugók, kétcsöves gáznyomásos lengéscsillapítással','irányíthatóság',5.4,114000,6);

insert into Tartalmaz
values('A200','motor','soros/4/115LE',1);
insert into Tartalmaz
values('A200','kerék','205/55 R 16',4);
insert into Tartalmaz
values('A200','váltó','7G-DCT',1);
insert into Tartalmaz
values('A200','fék elöl','belső hűtésű tárcsafék',2);
insert into Tartalmaz
values('A200','fék hátul','tömör tárcsafék',2);
insert into Tartalmaz
values('A200','rugózás','tekercsrugók, kétcsöves gáznyomásos lengéscsillapítással',4);

insert into Alkatresz
values('motor','soros/4/90LE','teljesítmény',4.6,238000,2);
insert into Alkatresz
values('kerék','195/65 R 15','irányíthatóság',4.7,78000,12);

insert into Tartalmaz
values('A180','motor','soros/4/90LE',1);
insert into Tartalmaz
values('A180','kerék','195/65 R 15',4);
insert into Tartalmaz
values('A180','váltó','7G-DCT',1);
insert into Tartalmaz
values('A180','fék elöl','belső hűtésű tárcsafék',2);
insert into Tartalmaz
values('A180','fék hátul','tömör tárcsafék',2);
insert into Tartalmaz
values('A180','rugózás','tekercsrugók, kétcsöves gáznyomásos lengéscsillapítással',4);

insert into Alkatresz
values('motor','soros/4/155LE','teljesítmény',5.2,318000,5);
insert into Alkatresz
values('kerék','225/45 R 17','irányíthatóság',6.1,96000,12);

insert into Tartalmaz
values('A250','motor','soros/4/155LE',1);
insert into Tartalmaz
values('A250','kerék','225/45 R 17',4);
insert into Tartalmaz
values('A250','váltó','7G-DCT',1);
insert into Tartalmaz
values('A250','fék elöl','belső hűtésű tárcsafék',2);
insert into Tartalmaz
values('A250','fék hátul','tömör tárcsafék',2);
insert into Tartalmaz
values('A250','rugózás','tekercsrugók, kétcsöves gáznyomásos lengéscsillapítással',4);

insert into Alkatresz
values('rugózás','csavarrugók, kétcsöves gáznyomásos lengéscsillapítással','irányíthatóság',6.6,136000,10);

insert into Tartalmaz
values('CLA250','motor','soros/4/155LE',1);
insert into Tartalmaz
values('CLA250','kerék','225/45 R 17',4);
insert into Tartalmaz
values('CLA250','váltó','7G-DCT',1);
insert into Tartalmaz
values('CLA250','fék elöl','belső hűtésű tárcsafék',2);
insert into Tartalmaz
values('CLA250','fék hátul','tömör tárcsafék',2);
insert into Tartalmaz
values('CLA250','rugózás','csavarrugók, kétcsöves gáznyomásos lengéscsillapítással',4);

insert into Alkatresz
values('váltó','7G-TRONIC PLUS','gyorsulás',6.2,153000,7);
insert into Alkatresz
values('kerék','225/50 R 17','irányíthatóság',6.3,98500,14);
insert into Alkatresz
values('rugózás','tekercsrugók, kétcsöves gáznyomásos lengéscsillapítással, SDD-vel','irányíthatóság',5.7,134000,6);


insert into Tartalmaz
values('C250','motor','soros/4/155LE',1);
insert into Tartalmaz
values('C250','kerék','225/50 R 17',4);
insert into Tartalmaz
values('C250','váltó','7G-TRONIC PLUS',1);
insert into Tartalmaz
values('C250','fék elöl','belső hűtésű tárcsafék',2);
insert into Tartalmaz
values('C250','fék hátul','tömör tárcsafék',2);
insert into Tartalmaz
values('C250','rugózás','tekercsrugók, kétcsöves gáznyomásos lengéscsillapítással, SDD-vel',4);

insert into Alkatresz
values('motor','soros/4/180LE','teljesítmény',5.7,348000,2);

insert into Tartalmaz
values('C300','motor','soros/4/180LE',1);
insert into Tartalmaz
values('C300','kerék','225/50 R 17',4);
insert into Tartalmaz
values('C300','váltó','7G-TRONIC PLUS',1);
insert into Tartalmaz
values('C300','fék elöl','belső hűtésű tárcsafék',2);
insert into Tartalmaz
values('C300','fék hátul','belső hűtésű tárcsafék',2);
insert into Tartalmaz
values('C300','rugózás','tekercsrugók, kétcsöves gáznyomásos lengéscsillapítással, SDD-vel',4);

insert into Alkatresz
values('váltó','9G-TRONIC automata','gyorsulás',7.1,197000,4);
insert into Alkatresz
values('rugózás','csavarrugók, egycsöves gáznyomásos lengéscsillapítással, SDD-vel','irányíthatóság',6.9,156000,10);
insert into Alkatresz
values('kerék','225/55 R 17','irányíthatóság',6.5,106000,14);

insert into Tartalmaz
values('E300','motor','soros/4/180LE',1);
insert into Tartalmaz
values('E300','kerék','225/55 R 17',4);
insert into Tartalmaz
values('E300','váltó','9G-TRONIC automata',1);
insert into Tartalmaz
values('E300','fék elöl','belső hűtésű tárcsafék',2);
insert into Tartalmaz
values('E300','fék hátul','belső hűtésű tárcsafék',2);
insert into Tartalmaz
values('E300','rugózás','csavarrugók, egycsöves gáznyomásos lengéscsillapítással, SDD-vel',4);


insert into Alkatresz
values('motor','V/6/245LE','teljesítmény',6.7,512000,2);
insert into Alkatresz
values('rugózás','csavarrugók, kétcsöves gáznyomásos lengéscsillapítással, SDD-vel','irányíthatóság',7.2,176000,6);
insert into Alkatresz
values('kerék','245/45 R 17','irányíthatóság',7.5,126000,8);
insert into Alkatresz
values('fék elöl','belső hűtésű, lyuggatott tárcsafék','irányíthatóság',7.0,112000,4);
insert into Alkatresz
values('fék hátul','belső hűtésű, lyuggatott tárcsafék','irányíthatóság',7.1,114000,6);


insert into Tartalmaz
values('CLS400','motor','V/6/245LE',1);
insert into Tartalmaz
values('CLS400','kerék','245/45 R 17',4);
insert into Tartalmaz
values('CLS400','váltó','7G-TRONIC PLUS',1);
insert into Tartalmaz
values('CLS400','fék elöl','belső hűtésű, lyuggatott tárcsafék',2);
insert into Tartalmaz
values('CLS400','fék hátul','belső hűtésű tárcsafék',2);
insert into Tartalmaz
values('CLS400','rugózás','csavarrugók, kétcsöves gáznyomásos lengéscsillapítással, SDD-vel',4);

insert into Alkatresz
values('motor','V/6/270LE','teljesítmény',7.1,620000,1);
insert into Alkatresz
values('váltó','9G-TRONIC','gyorsulás',7.1,197000,4);
insert into Alkatresz
values('kerék','255/40 R 18','irányíthatóság',7.9,138000,8);
insert into Alkatresz
values('rugózás','csavarrugók, egycsöves lengéscsillapítással,KVD-vel','irányíthatóság',7.7,196000,6);


insert into Tartalmaz
values('SL400','motor','V/6/270LE',1);
insert into Tartalmaz
values('SL400','kerék','255/40 R 18',4);
insert into Tartalmaz
values('SL400','váltó','9G-TRONIC',1);
insert into Tartalmaz
values('SL400','fék elöl','belső hűtésű, lyuggatott tárcsafék',2);
insert into Tartalmaz
values('SL400','fék hátul','belső hűtésű tárcsafék',2);
insert into Tartalmaz
values('SL400','rugózás','csavarrugók, egycsöves lengéscsillapítással,KVD-vel',4);


insert into Alkatresz
values('motor','V/8/340LE','teljesítmény',8.3,789000,1);
insert into Alkatresz
values('váltó','AMG SPEEDSHIFT DCT 7 sebességes','gyorsulás',8.2,267000,2);
insert into Alkatresz
values('kerék','255/35 R 19','irányíthatóság',8.1,158000,8);


insert into Tartalmaz
values('AMG GT','motor','V/8/340LE',1);
insert into Tartalmaz
values('AMG GT','kerék','255/35 R 19',4);
insert into Tartalmaz
values('AMG GT','váltó','AMG SPEEDSHIFT DCT 7 sebességes',1);
insert into Tartalmaz
values('AMG GT','fék elöl','belső hűtésű, lyuggatott tárcsafék',2);
insert into Tartalmaz
values('AMG GT','fék hátul','belső hűtésű, lyuggatott tárcsafék',2);
insert into Tartalmaz
values('AMG GT','rugózás','tekercsrugók, kétcsöves gáznyomásos lengéscsillapítással',4);

insert into Alkatresz
values('rugózás','tekercsrugók, kétcsöves lengéscsillapítással,CDC-vel','irányíthatóság',8.4,276000,2);
insert into Alkatresz
values('rugózás','tekercsrugók, egycsöves lengéscsillapítással,CDC-vel','irányíthatóság',8.2,256000,2);
insert into Alkatresz
values('motor','V/8/430LE','teljesítmény',8.8,1210000,2);
insert into Alkatresz
values('motor','V/12/463LE','teljesítmény',9.2,1512000,0);

insert into Alkatresz
values('turbo','M266E20LA','gyorsulás',5.3,140000,2);
insert into Alkatresz
values('turbo','OMC250','gyorsulás',6.3,162000,2);
insert into Alkatresz
values('turbo','OME300','gyorsulás',6.7,182000,2);
insert into Alkatresz
values('turbo','OMSL4','gyorsulás',8.2,262000,1);
insert into Alkatresz
values('turbo','OMAMG1GT','gyorsulás',9.3,368000,0);
