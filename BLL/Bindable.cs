﻿namespace BLL
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Adatkötéshez szükséges segédosztály
    /// </summary>
    public abstract class Bindable : INotifyPropertyChanged
    {
        #region Events

        /// <summary>
        /// ProperyChanged esemény
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Methods

        /// <summary>
        /// OnPropertyChanged metódus
        /// </summary>
        /// <param name="propertyName">tulajdonság neve</param>
        protected void OnPorpertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}