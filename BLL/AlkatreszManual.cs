﻿namespace BLL
{
    using DAL;

    /// <summary>
    /// Az osztály, ami lehetővé teszi a kiválasztott autó alkatrészeinek árainak összegszését
    /// </summary>
    public class AlkatreszManual : Bindable
    {
        #region Fields

        /// <summary>
        /// A checkbox IsChecked tulajdonságát reprezentáló mező
        /// </summary>
        private bool szukseges;

        #endregion

        #region Properties

        /// <summary>
        /// A "szukseges" mezőt visszaadó tulajdonság
        /// </summary>
        public bool Szukseges
        {
            get
            {
                return szukseges;
            }

            set
            {
                szukseges = value;
                OnPorpertyChanged();
            }
        }

        /// <summary>
        /// Alkatrész
        /// </summary>
        public Alkatresz Alkatresz { get; set; }

        #endregion
    }
}
