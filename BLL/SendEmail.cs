﻿namespace BLL
{
    using System;
    using System.Net;
    using System.Net.Mail;

    /// <summary>
    /// sendemail osztály
    /// </summary>
    public class SendEmail
    {
        /// <summary>
        /// Email küldő metódus
        /// </summary>
        /// <param name="email">ügyfél emailje</param>
        /// <param name="passw">ügyfél jelszava</param>
        public static void SendMail(string email, string passw)
        {
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();

                message.From = new MailAddress("nik.bruteforce@gmail.com");
                message.To.Add(new MailAddress(email));
                message.Subject = "Üdvözöljük a Tuning Zrt.-nél";
                message.Body = "Kedves Ügyfelünk. Üdvözöljük! \nAz Őn regisztrált email címe : " + email + ", az Őn jelszava : " + passw + ". \n Jó böngészést kívánunk! Ha bármi kérdése van állunk szolálatára! \nÜdvözlettel, a Tuning Zrt. csapata.";
                smtp.Port = 587;
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("nik.bruteforce@gmail.com", "n1kbrute");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
            }
            catch (Exception)
            {
            }
        }
    }
}
