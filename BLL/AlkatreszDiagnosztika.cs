﻿namespace BLL
{
    using DAL;

    /// <summary>
    /// A diagnosztikához szükséges segédosztály
    /// </summary>
    public class AlkatreszDiagnosztika
    {
        #region Constructors

        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="k">kell</param>
        /// <param name="a">alkatrész</param>
        public AlkatreszDiagnosztika(bool k, Alkatresz a)
        {
            Kell = k;
            Alk = a;
        }

        #endregion
        #region Fields

        /// <summary>
        /// Igaz, ha az adott alkatrész része az optimális konfigurációnak
        /// </summary>
        public bool Kell { get; set; }

        #endregion

        #region Properties

        /// <summary>
        /// Az alkatrészt reprezentáló mező
        /// </summary>
        public Alkatresz Alk { get; set; }

        #endregion
    }
}
