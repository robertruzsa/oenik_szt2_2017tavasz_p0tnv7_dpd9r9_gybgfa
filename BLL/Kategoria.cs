﻿namespace BLL
{
    /// <summary>
    /// Az alkatrész kategóriáját reprezentáló osztály
    /// </summary>
    public class Kategoria : Bindable
    {
        #region Fields

        /// <summary>
        /// Igaz, ha a fejlesztési kategória ki van választva
        /// </summary>
        private bool szukseges;

        #endregion

        #region Properties

        /// <summary>
        /// A fejlesztési kategória neve
        /// </summary>
        public string KategoriaNev { get; set; }

        /// <summary>
        /// A "szukseges" mező tulajdonsága
        /// </summary>
        public bool Szukseges
        {
            get
            {
                return szukseges;
            }

            set
            {
                szukseges = value;
                OnPorpertyChanged();
            }
        }

        #endregion
    }
}
