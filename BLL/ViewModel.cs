﻿namespace BLL
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using DAL;

    /// <summary>
    /// ViewModel
    /// </summary>
    public class ViewModel : Bindable
    {
        #region Fields
        /// <summary>
        /// A diagnosztika eredményét tárolja
        /// </summary>
        private BindingList<Alkatresz> optimalisKonfiguracio;

        /// <summary>
        /// Tartalmazza a raktáron lévő alkatrészeket
        /// </summary>
        private BindingList<Alkatresz> alkatreszekRaktaron;

        #endregion
        #region Constructor

        /// <summary>
        /// A ViewModel konstruktora
        /// </summary>
        public ViewModel()
        {
            DE = DatabaseMethods.Init();
            Autok = new List<Auto>();

            OptimalisKonfiguracio = new BindingList<Alkatresz>();

            AlkatreszekRaktaron = new BindingList<Alkatresz>();

            foreach (var auto in DE.Auto)
            {
                Autok.Add(auto);
            }

            Alkatreszek = new List<Alkatresz>();
            foreach (var alkatresz in DE.Alkatresz)
            {
                Alkatreszek.Add(alkatresz);
                AlkatreszekRaktaron.Add(alkatresz);
            }

            Kategoriak = new BindingList<Kategoria>()
            {
                new Kategoria() { KategoriaNev = "Irányíthatóság" },
                new Kategoria() { KategoriaNev = "Teljesítmény" },
                new Kategoria() { KategoriaNev = "Gyorsulás" }
            };

            AlkMan = new BindingList<AlkatreszManual>();
            foreach (Alkatresz a in DatabaseMethods.OsszAlkatresz(DE))
            {
                AlkMan.Add(new AlkatreszManual() { Szukseges = false, Alkatresz = a });
            }

            AlkMan.ListChanged += AlkMan_ListChanged;

            KartyaTipusok = new List<string>()
            {
                "MasterCard", "Maestro", "Erzsébet-utalvány Plusz", "VISA", "American Express", "OTP SZÉP kártya", "OTP Cafetéria kártya", "Egyéb"
            };

            Kategoriak.ListChanged += Kategoriak_ListChanged;
            OptimalisKonfiguracio.ListChanged += OptimalisKonfiguracio_ListChanged;
        }
        #endregion        

        #region Properties
        /// <summary>
        /// viewmodel
        /// </summary>
        private static ViewModel vmx;

        /// <summary>
        /// database entites
        /// </summary>
        public DatabaseEntities DE { get; set; }

        /// <summary>
        /// autók listája
        /// </summary>
        public List<Auto> Autok { get; set; }

        /// <summary>
        /// Az ügyfél autója
        /// </summary>
        public Auto KivalasztottAuto { get; set; }

        /// <summary>
        /// A kiválasztott autó alkatrészei
        /// </summary>
        public List<Alkatresz> Alkatreszek { get; set; }

        /// <summary>
        /// Alkatrész rendelése során kiválasztott alkatrész
        /// </summary>
        public Alkatresz KivalasztottAlkatresz { get; set; }

        /// <summary>
        /// Az ügyfél által választott kategóriák
        /// </summary>
        public BindingList<Kategoria> Kategoriak { get; set; }

        /// <summary>
        /// A kiválasztott kategóriában az ügyfél autójának alkatrészeinél "jobb" alkatrészek
        /// </summary>
        public List<Alkatresz> SzurtAlkatreszek
        {
            get
            {
                List<Alkatresz> seged = new List<Alkatresz>();
                List<Alkatresz> szurtAlkatreszek = new List<Alkatresz>();

                Alkatreszek = DatabaseMethods.AlkatreszekLekerdezese(KivalasztottAuto.au_tipus, DE);

                foreach (Kategoria k in Kategoriak)
                {
                    if (k.Szukseges)
                    {
                        seged = DatabaseMethods.KategorizaltAlkatreszekLekerdezese(k.KategoriaNev, DE);
                        szurtAlkatreszek = szurtAlkatreszek.Union(seged).Cast<Alkatresz>().ToList();
                    }
                }

                List<Alkatresz> vissza = new List<Alkatresz>();

                foreach (Alkatresz startAlk in Alkatreszek)
                {
                    foreach (Alkatresz szurtAlk in szurtAlkatreszek)
                    {
                        if (startAlk.nev == szurtAlk.nev && szurtAlk.ertek > startAlk.ertek)
                        {
                            vissza.Add(szurtAlk);
                        }

                        if (startAlk.nev != szurtAlk.nev && startAlk.kategoria == szurtAlk.kategoria && szurtAlk.ertek > startAlk.ertek)
                        {
                            vissza.Add(szurtAlk);
                        }
                    }
                }

                return vissza.Union(new List<Alkatresz>()).Cast<Alkatresz>().OrderByDescending(x => x.ertek).ToList();
            }
        }

        /// <summary>
        /// visszaadja az optimális konfigurációt
        /// </summary>
        public BindingList<Alkatresz> OptimalisKonfiguracio
        {
            get
            {
                return optimalisKonfiguracio;
            }

            set
            {
                optimalisKonfiguracio = value;
                OnPorpertyChanged("OptimalisKonfiguracio");
                OnPorpertyChanged("TeljesAr");
            }
        }

        /// <summary>
        /// visszaadja az optimális konfigurációt alkotó alkatrészek árainak összegét
        /// </summary>
        public int TeljesAr
        {
            get
            {
                int sum = 0;
                foreach (Alkatresz a in vmx.optimalisKonfiguracio)
                {
                    sum += (int)a.eladasi_ar;
                }

                return sum;
            }
        }

        /// <summary>
        /// Az ügyfél által kiválasztott alkatrészek adatkötéséhez szükséges tulajdonság
        /// </summary>
        public BindingList<AlkatreszManual> AlkMan { get; set; }

        /// <summary>
        /// Az ügyfél által kiválasztott alkatrészek árainak összege
        /// </summary>
        public int TeljesArManual
        {
            get
            {
                int sum = 0;
                foreach (AlkatreszManual am in AlkMan)
                {
                    if (am.Szukseges)
                    {
                        sum += (int)am.Alkatresz.eladasi_ar;
                    }
                }

                return sum;
            }
        }

        /// <summary>
        /// Visszaadja a raktáron lévő alkatrészeket
        /// </summary>
        public BindingList<Alkatresz> AlkatreszekRaktaron
        {
            get
            {
                return alkatreszekRaktaron;
            }

            set
            {
                alkatreszekRaktaron = value;
                OnPorpertyChanged("AlkatreszekRaktaron");
            }
        }

        /// <summary>
        /// A fizetéskor választható kártyatípusok listája
        /// </summary>
        public List<string> KartyaTipusok { get; set; }

        #endregion
        #region Methods
        /// <summary>
        /// Az ügyfél által kiválasztott alkatrészek listáját nyomonkövető esemény
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">r</param>
        public void AlkMan_ListChanged(object sender, ListChangedEventArgs e)
        {
            OnPorpertyChanged("TeljesArManual");
        }
        
        /// <summary>
        /// Egyke tervezési mintához szükséges metódus
        /// </summary>
        /// <returns>Visszaadja a már létező ViewModelt, egyébként létrehozza</returns>
        public static ViewModel Get()
        {
            if (vmx == null)
                vmx = new ViewModel();
            return vmx;
        }

        #endregion

        #region Events

        /// <summary>
        /// Az optimális konfiguráció változásait nyomonkövető esemény
        /// </summary>
        /// <param name="sender">s</param>
        /// <param name="e">e</param>
        private void OptimalisKonfiguracio_ListChanged(object sender, ListChangedEventArgs e)
        {
            OnPorpertyChanged("OptimalisKonfiguracio");
        }

        /// <summary>
        /// A "kategóriák" lista változásait nyomonkövető esemény
        /// </summary>
        /// <param name="sender">s</param>
        /// <param name="e">e</param>
        private void Kategoriak_ListChanged(object sender, ListChangedEventArgs e)
        {
            OnPorpertyChanged("SzurtAlkatreszek");
        }
        #endregion
    }
}