﻿namespace BLL
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using DAL;

    /// <summary>
    /// Az üzleti logikáért felelős osztály
    /// </summary>
    public class BusinessLogic
    {
        #region Constructor

        /// <summary>
        /// A BusinessLogic konstruktora
        /// </summary>
        public BusinessLogic()
        {
            VM = ViewModel.Get();
        }

        #endregion

        #region Properties

        /// <summary>
        /// BusinessLogic
        /// </summary>
        private static BusinessLogic blx;

        /// <summary>
        /// ViewModel
        /// </summary>
        public ViewModel VM { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Az egyke tervezési mintához szükséges metódus
        /// </summary>
        /// <returns>Visszaadja a már létező BL-t, egyébként létrehozza</returns>
        public static BusinessLogic Get()
        {
            if (blx == null)
                blx = new BusinessLogic();
            return blx;
        }

        /// <summary>
        /// Meghívja a metódust, amivel új alkatrész adható hozzá az adatbázishoz
        /// </summary>
        /// <param name="nev">alkatrész neve</param>
        /// <param name="tipus">típusa</param>
        /// <param name="kat">kategóriája</param>
        /// <param name="ert">értéke</param>
        /// <param name="menny">mennyisége</param>
        /// <param name="ar">eladási ára</param>
        public void UjAlkatresz(string nev, string tipus, string kat, int ert, int menny, int ar)
        {
            DatabaseMethods.HozzaAdas(VM.DE, nev, tipus, kat, ert, menny, ar);
        }

        /// <summary>
        /// Metódus, amivel alkatrész rendelhető
        /// </summary>
        /// <param name="alkatresz">alkatrész</param>
        /// <param name="menny">rendelni kívánt mennyiség</param>
        public void Rendeles(Alkatresz alkatresz, int menny)
        {
            DatabaseMethods.Valtoztatas(alkatresz, menny, blx.VM.DE);
        }

        /// <summary>
        /// Az alkatrész rendelés adatkötéséhez szükséges metódus
        /// </summary>
        /// <param name="alkatresz">alkatrész</param>
        /// <param name="menny">mennyiség</param>
        /// <returns>Alkatrész list</returns>
        public BindingList<Alkatresz> AlkatreszRendeles(Alkatresz alkatresz, int menny)
        {
            Rendeles(alkatresz, menny);
            BindingList<Alkatresz> raktar = new BindingList<Alkatresz>();
            foreach (Alkatresz a in blx.VM.DE.Alkatresz)
            {
                raktar.Add(a);
            }

            return raktar;
        }

        /// <summary>
        /// Meghívja a metódust, ami egy konkrét kategória szerint szűri az alkatrészeket
        /// </summary>
        /// <param name="kategoria">kategória</param>
        /// <returns>KategorizaltAlkatreszek</returns>
        public List<Alkatresz> KategorizaltAlkatreszek(string kategoria)
        {
            return DatabaseMethods.KategorizaltAlkatreszekLekerdezese(kategoria, blx.VM.DE);
        }

        /// <summary>
        /// Létrehoz egy tömböt, ami tartalmazza azokat az alkatrészeket, amelyek "jobbak", mint amelyeket az ügyfél autója tartalmaz
        /// </summary>
        /// <param name="osszAlk">összes alkatrészt tároló lista</param>
        /// <returns>OptInit</returns>
        public AlkatreszDiagnosztika[] OptInit(List<Alkatresz> osszAlk)
        {
            AlkatreszDiagnosztika[] opt = new AlkatreszDiagnosztika[blx.VM.SzurtAlkatreszek.Count];
            int i = 0;
            foreach (Alkatresz a in osszAlk)
            {
                opt[i] = new AlkatreszDiagnosztika(false, a);
                i++;
            }

            return opt;
        }

        /// <summary>
        /// A diagnosztika során fejleszteni kívánt alkatrészek árainak összegét adja vissza
        /// </summary>
        /// <param name="s">Az alkatrészeket tartalmazó tömb</param>
        /// <returns>összeg</returns>
        public int OsszAr(AlkatreszDiagnosztika[] s)
        {
            int sum = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i].Kell)
                {
                    sum += (int)s[i].Alk.eladasi_ar;
                }
            }

            return sum;
        }

        /// <summary>
        /// Az optimalizációhoz szükséges segédmetódus
        /// </summary>
        /// <param name="s">az alkatrészeket tartalamzó tömb</param>
        /// <returns>összérték</returns>
        public int OsszErtek(AlkatreszDiagnosztika[] s)
        {
            int sum = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i].Kell)
                {
                    sum += (int)s[i].Alk.ertek;
                }
            }

            return sum;
        }

        /// <summary>
        /// Az optimalizációhoz szükséges segédmetódus
        /// </summary>
        /// <param name="s">az alkatrészeket tartalamzó tömb</param>
        /// <returns>visszaadja, hogy az alkatrészek száma megfelelő e</returns>
        public bool AlkatreszekSzamaMegfelelo(AlkatreszDiagnosztika[] s)
        {
            int sum = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i].Kell)
                {
                    sum++;
                }
            }

            if (blx.VM.Kategoriak.ElementAt(2).Szukseges)
            {
                return sum < 7;
            }
            else
            {
                return sum < 6;
            }
        }

        /// <summary>
        /// konvertáló metódus
        /// </summary>
        /// <param name="s">az alkatrészeket tartalamzó tömb</param>
        /// <returns>lista</returns>
        public BindingList<Alkatresz> SegedToAlkatresz(AlkatreszDiagnosztika[] s)
        {
            BindingList<Alkatresz> alk = new BindingList<Alkatresz>();
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i].Kell)
                {
                    alk.Add(s[i].Alk);
                }
            }

            return alk;
        }

        /// <summary>
        /// Az optimalizációhoz szükséges segédmetódus
        /// </summary>
        /// <param name="s">alkatrész</param>
        /// <param name="seg">az alkatrészeket tartalamzó tömb</param>
        /// <returns>VanEMar</returns>
        public bool VanEMar(AlkatreszDiagnosztika s, AlkatreszDiagnosztika[] seg)
        {
            int j = 0;
            for (int i = 0; i < seg.Length; i++)
            {
                if (s.Alk.nev == seg[i].Alk.nev && seg[i].Kell)
                {
                    j++;
                }
            }

            return j > 0;
        }

        /// <summary>
        /// A fejlesztést megvalósító metódus, azaz az optimalizáció
        /// </summary>
        /// <param name="osszeg">fejlesztésre szánt összeg maximuma</param>
        /// <returns>visszaadja az optimális konfigurációt tartalmazó listát</returns>
        public BindingList<Alkatresz> Fejlesztes(int osszeg)
        {
            AlkatreszDiagnosztika[] opt = OptInit(blx.VM.SzurtAlkatreszek);
            int n = opt.Length;
            int t = 0;
            while (OsszAr(opt) <= osszeg && AlkatreszekSzamaMegfelelo(opt) && t < opt.Length)
            {
                if (OsszAr(opt) + opt[t].Alk.eladasi_ar <= osszeg)
                {
                    if (!VanEMar(opt[t], opt))
                    {
                        opt[t].Kell = true;
                    }
                }

                t++;
            }

            return SegedToAlkatresz(opt);
        }

        #endregion
    }
}
