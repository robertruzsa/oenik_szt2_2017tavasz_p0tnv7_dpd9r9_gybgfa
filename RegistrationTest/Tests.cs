﻿namespace RegistrationTest
{
    using DAL;
    using NUnit.Framework;

    /// <summary>
    /// Tesztek
    /// </summary>
    [TestFixture]
    public class Tests
    {
        [Test]
        [TestCase("admin","admin")]
        [TestCase(null,null)]
        [TestCase("admin",null)]
        [TestCase(null,"valami")]
        [TestCase("user","password")]        
        public void InsertToDBTest(string email, string password)
        {
            bool temp;
            temp = UsersDB.InsertToDB(email, password);
            Assert.That(temp, Is.EqualTo(true));
        }

        [Test]
        [TestCase("admin")]
        [TestCase("null")]
        [TestCase("istvan")]
        public void UserIsExistingTest(string email)
        {
            bool temp;
            temp = UsersDB.UserIsExisting(email);
            Assert.That(temp, Is.EqualTo(true));
        }
    }
}
