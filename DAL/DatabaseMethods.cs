﻿namespace DAL
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Adatbázis műveleteit tartalmazó osztály
    /// </summary>
    public class DatabaseMethods
    {
        #region Properties

        /// <summary>
        /// Gets Az adatbázis reprezentáló tulajdonság
        /// </summary>
        private static DatabaseEntities DE { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Inicializálja az adatbázist reprezentáló osztályt
        /// </summary>
        /// <returns>DE</returns>
        public static DatabaseEntities Init()
        {
            return DE = new DatabaseEntities();
        }

        /// <summary>
        /// Adatbázishoz hozzáadás
        /// </summary>
        /// <param name="DE">DE</param>
        /// <param name="nev">alkatrész neve</param>
        /// <param name="tipus">típus</param>
        /// <param name="kat">kategória</param>
        /// <param name="ert">érték</param>
        /// <param name="menny">mennyiség</param>
        /// <param name="ar">eladási ár</param>
        public static void HozzaAdas(DatabaseEntities DE, string nev, string tipus, string kat, int ert, int menny, int ar)
        {
            var newElem = new Alkatresz()
            {
                nev = nev,
                alk_tipus = tipus,
                kategoria = kat,
                ertek = ert,
                mennyiseg = menny,
                eladasi_ar = ar
            };
            DE.Alkatresz.Add(newElem);
            DE.SaveChanges();
        }

        /// <summary>
        /// Az adatbázisból lekérdezi egy listába az adott autó alkatrészeit
        /// </summary>
        /// <param name="auto">autó típusa</param>
        /// <param name="DE">DE</param>
        /// <returns>AlkatreszekLekerdezese</returns>
        public static List<Alkatresz> AlkatreszekLekerdezese(string auto, DatabaseEntities DE)
        {
            List<Alkatresz> alkatreszek = (from akt in DE.Tartalmaz
                                           where akt.au_tipus == auto
                                           from akt2 in DE.Alkatresz
                                           where akt2.alk_tipus == akt.alk_tipus && akt2.nev == akt.nev
                                           select akt2).ToList();
            return alkatreszek;
        }

        /// <summary>
        /// Kategória szerint kérdezi le az alkatrészeket az adatbázisból
        /// </summary>
        /// <param name="kategoria">kategória</param>
        /// <param name="DE">DE</param>
        /// <returns>KategorizaltAlkatreszekLekerdezese</returns>
        public static List<Alkatresz> KategorizaltAlkatreszekLekerdezese(string kategoria, DatabaseEntities DE)
        {
            List<Alkatresz> kategorizaltAlkatreszek = (from akt in DE.Alkatresz
                                                       where akt.kategoria == kategoria
                                                       select akt).ToList();
            return kategorizaltAlkatreszek;
        }

        /// <summary>
        /// Lekérdezi egy listába az adatbázis összes alkatrészét
        /// </summary>
        /// <param name="DE">DE</param>
        /// <returns>az alkatrészek listája</returns>
        public static List<Alkatresz> OsszAlkatresz(DatabaseEntities DE)
        {
            List<Alkatresz> osszAlkatresz = (from akt in DE.Alkatresz
                                             select akt).ToList();
            return osszAlkatresz.OrderByDescending(x => x.ertek).ToList();
        }

        /// <summary>
        /// Adatbázis módosítása (alkatrész mennyisége)
        /// </summary>
        /// <param name="alkatresz">alkatrész</param>
        /// <param name="menny">mennyiség</param>
        /// <param name="DE">DE</param>
        public static void Valtoztatas(Alkatresz alkatresz, int menny, DatabaseEntities DE)
        {
            var seged = DE.Alkatresz.Single(x => x.nev == alkatresz.nev && x.alk_tipus == alkatresz.alk_tipus);
            seged.mennyiseg += menny;
            DE.SaveChanges();
        }

        #endregion
    }
}
