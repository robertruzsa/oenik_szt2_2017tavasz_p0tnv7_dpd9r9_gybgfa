//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Alkatresz
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Alkatresz()
        {
            this.Tartalmaz = new HashSet<Tartalmaz>();
        }
    
        public string nev { get; set; }
        public string alk_tipus { get; set; }
        public string kategoria { get; set; }
        public Nullable<decimal> ertek { get; set; }
        public Nullable<decimal> eladasi_ar { get; set; }
        public Nullable<decimal> mennyiseg { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tartalmaz> Tartalmaz { get; set; }
    }
}
