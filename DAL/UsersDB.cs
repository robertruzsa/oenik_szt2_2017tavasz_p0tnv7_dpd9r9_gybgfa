﻿/// <summary>
/// Database Layer
/// </summary>
namespace DAL
{
    using System;
    using System.Linq;

    /// <summary>
    /// Felhasznalok adatbazis osztalya
    /// </summary>
    public class UsersDB
    {
        /// <summary>
        /// UsersDBEntinites peldanyositasa
        /// </summary>
        public static UsersDataBaseEntities UD = new UsersDataBaseEntities();

        /// <summary>
        /// Ez a metodus az adatbazisba valo beillesztest vegzi el
        /// </summary>
        /// <param name="email">felhasznalo emailje</param>
        /// <param name="password">felhasznalo jelszava</param>
        /// <returns>true ha sikeres</returns>
        public static bool InsertToDB(string email, string password)
        {
            var newUser = new Users()
            {
                email = email,
                password = password
            };
            UD.Users.Add(newUser);
            try
            {
                UD.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Letezik-e a megadott email cimu felhasznalo
        /// </summary>
        /// <param name="email">felhasznalo emailje</param>
        /// <returns>true ha van ilyen felhasznalo</returns>
        public static bool UserIsExisting(string email)
        {
            bool isExisting = false;
            foreach (var user in UD.Users.Where(Users => Users.email == email))
            {
                isExisting = true;
            }

            return isExisting;
        }

        /// <summary>
        /// adatbazisbol jelszo lekerese emailcimhez
        /// </summary>
        /// <param name="email">felhasznalo emailcime</param>
        /// <returns>password a felhasznalohoz</returns>
        public static string PasswordFromDB(string email)
        {
            string pass = null;
            foreach (var user in UD.Users.Where(Users => Users.email == email))
            {
                pass = user.password;
            }

            return pass;
        }
    }
}
